import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { LoginModule } from './features/login/login.module';
import { RegistrationModule } from './features/registration/registration.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule} from '@angular/common/http';
import { WindowProvider } from 'window-token';
import { AuthorizedGuard } from './auth/guards/authorized.guard';
import { NotAuthorizedGuard } from './auth/guards/not-authorized.guard';
import { RouterModule } from '@angular/router';
import { HeaderModule } from './features/header/header.module';
import { effects, reducers } from './store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HeaderModule,
    SharedModule,
    LoginModule,
    RegistrationModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument(),
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    AuthorizedGuard,
    NotAuthorizedGuard,
    WindowProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
