import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthStateFacade } from './auth/store/auth.facade';
import { UserStateFacade } from './user/store/user.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAuthorized$: Observable<boolean>;
  name$: Observable<string>;
  isAuthorized: boolean = false;

  constructor(
    private authFacade: AuthStateFacade,
    private userFacade: UserStateFacade
  ) { }

  ngOnInit() {
    this.isAuthorized$ = this.authFacade.isAuthorized$;
    this.name$ = this.userFacade.name$;
  }

  logout() {
    this.authFacade.logout();
  }
}
