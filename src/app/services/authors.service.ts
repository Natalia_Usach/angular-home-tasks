import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get('http://localhost:3000/authors/all');
  }

  addAuthor(author: string) {
    return this.http.post('http://localhost:3000/authors/add', { name: author });
  }

  deleteAuthor(id: string) {
    return this.http.delete(`http://localhost:3000/authors/${id}`);
  }
}
