import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get('http://localhost:3000/courses/all');
  }

  getCourse(id: string): Observable<any> {
    return this.http.get(`http://localhost:3000/courses/${id}`);
  }

  deleteCourse(id: string): Observable<any> {
    return this.http.delete(`http://localhost:3000/courses/${id}`);
  }

  addCourse(course: Course) {
    return this.http.post('http://localhost:3000/courses/add', course);
  }

  editCourse(course: Course, id: string) {
    return this.http.put(`http://localhost:3000/courses/${id}`, course);
  }

  searchCourse(params: HttpParams) {
    return this.http.get('http://localhost:3000/courses/filter', { params });
  }
}
