import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringJoiner'
})
export class StringJoinerPipe implements PipeTransform {

  transform(arr: string[]): string {
    return arr?.length === 1 ? arr[0] : arr?.join(', ');
  }
}
