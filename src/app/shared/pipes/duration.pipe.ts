import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'durationPipe'
})
export class DurationPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    let time = new Date(value * 60 * 1000).toISOString().substr(11, 5);
    if(time.startsWith('00') || time.startsWith('01')) {
      return time + ' hour';
    }
    return time + ' hours';
  }

}
