import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonComponent } from './components/button/button.component';
import { InfoComponent } from './components/info/info.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule } from '@angular/forms';
import { EmailValidationDirective } from './validators/email-validation.directive';
import { AuthorValidationDirective } from './validators/author-validation.directive';
import { DurationPipe } from './pipes/duration.pipe';
import { CreationDatePipe } from './pipes/creation-date.pipe';
import { StringJoinerPipe } from './pipes/string-joiner.pipe';

const componentsArray = [
  ButtonComponent,
  InfoComponent,
  SearchComponent
];

@NgModule({
  declarations: [
    ...componentsArray, 
    EmailValidationDirective, 
    AuthorValidationDirective, 
    DurationPipe, 
    CreationDatePipe, 
    StringJoinerPipe
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule
  ],
  exports: [
    ...componentsArray, 
    CommonModule, 
    FontAwesomeModule, 
    EmailValidationDirective,
    DurationPipe, 
    CreationDatePipe, 
    StringJoinerPipe
  ]
})
export class SharedModule { }
