import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function createNewAuthorValidator(): ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }

        const re = /[a-zA-Z0-9]+/;
        const newAuthorValid = re.test(value);
        return !newAuthorValid ? {appAuthorValidation: true}: null;
    }
}