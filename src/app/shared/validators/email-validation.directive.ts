import { Directive } from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import { createEmailValidator } from './email-validator';

@Directive({
  selector: '[appEmailValidation]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: EmailValidationDirective,
    multi: true
  }]
})
export class EmailValidationDirective implements Validator{

  validate(control: AbstractControl): ValidationErrors | null {
    return createEmailValidator()(control);
  }
}
