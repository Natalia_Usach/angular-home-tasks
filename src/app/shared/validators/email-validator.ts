import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function createEmailValidator(): ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }

        const re = /\S+@\S+\.\S+/;
        const emailValid = re.test(value);
        return !emailValid ? {appEmailValidation: true}: null;
    }
}