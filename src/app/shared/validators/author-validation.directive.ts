import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { createNewAuthorValidator } from './new-author-validator';

@Directive({
  selector: '[appAuthorValidation]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: AuthorValidationDirective,
    multi: true
  }]
})
export class AuthorValidationDirective {

  validate(control: AbstractControl): ValidationErrors | null {
    return createNewAuthorValidator()(control);
  }
}
