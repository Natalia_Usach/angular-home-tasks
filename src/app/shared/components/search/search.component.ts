import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @Input() placeholder: string;
  @Input() shouldFilter: boolean;
  @Output() findCourse = new EventEmitter();
  @Output() clearSearch = new EventEmitter();

  @ViewChild('search') search: ElementRef;

  text = 'Search';
  searchInput: string = '';

  onSearchClick(query?: string) {
    if (query) {
      this.findCourse.emit(query);
    }
  }
  onClearSearchClick() {
    this.clearSearch.emit(true);
    this.search.nativeElement.value = '';
  }
}
