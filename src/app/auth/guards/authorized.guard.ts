import { Injectable } from '@angular/core';
import { CanLoad, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStateFacade } from '../store/auth.facade';

@Injectable({
  providedIn: 'root'
})

export class AuthorizedGuard implements CanLoad {
  isAuthorized: boolean;

  constructor(private router: Router, private authFacade: AuthStateFacade) { }

  canLoad(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.authFacade.isAuthorized$.subscribe(auth => {
      this.isAuthorized = auth;
    });
    return this.isAuthorized ? true : this.router.createUrlTree(['/login']);
  }
}
