import { createFeatureSelector, createSelector } from "@ngrx/store";
import { authFeatureKey, AuthState } from "./auth.reducer";

export const featureSelector = createFeatureSelector<AuthState>(authFeatureKey);

export const isUserAuthorized = createSelector(
  featureSelector,
  state => state.isAuthorized
);

export const getToken = createSelector(
  featureSelector,
  state => state.token
);

export const getLoginErrorMessage = createSelector(
  featureSelector,
  state => state.errorMessage
);

export const getRegisterErrorMessage = createSelector(
  featureSelector,
  state => state.errorMessage
);