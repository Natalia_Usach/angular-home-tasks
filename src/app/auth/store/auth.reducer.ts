import { Action, createReducer, on } from "@ngrx/store";
import { requestLogin, requestLoginFail, requestLoginSuccess, requestLogout, requestLogoutSuccess, requestRegister, requestRegisterFail, requestRegisterSuccess } from "./auth.actions";

export const authFeatureKey = 'auth';

export interface AuthState {
  isAuthorized: boolean,
  token: string | null,
  errorMessage?: string | null
}

export const initialState: AuthState = {
  isAuthorized: false,
  token: null,
  errorMessage: null
};

export const authUserReducer = createReducer(
  initialState,
  on(requestLogin, (state) => {
    return {
    ...state
  }}),
  on(requestLoginSuccess, (state, props) => ({
    ...state,
    isAuthorized: true,
    token: props.token
  })),
  on(requestLoginFail, (state) => ({
    ...state,
    isAuthorized: false,
    errorMessage: 'Login failure'
  })),
  on(requestRegister, state => ({
    ...state
  })),
  on(requestRegisterSuccess, (state) => ({
    ...state,
    errorMessage: null
  })),
  on(requestRegisterFail, (state) => ({
    ...state,
    errorMessage: "Registration failure"
  })),
  on(requestLogout, (state) => ({
    ...state
  })),
  on(requestLogoutSuccess, (state) => ({
    ...state,
    isAuthorized: false,
    token: null
  }))
);

export const authReducer = (state: AuthState, action: Action): AuthState => authUserReducer(state, action);
  