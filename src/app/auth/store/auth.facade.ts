import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { requestCurrentUserLogout } from "src/app/user/store/user.actions";
import { UserState } from "src/app/user/store/user.reducer";
import { UserModel } from "src/app/user/user.model";
import { SessionStorageService } from "../services/session-storage.service";
import { requestLogin, requestLoginSuccess, requestLogout, requestLogoutSuccess, requestRegister } from "./auth.actions";
import { AuthState } from "./auth.reducer";
import { getLoginErrorMessage, getRegisterErrorMessage, getToken, isUserAuthorized } from "./auth.selectors";

@Injectable({
  providedIn: 'root',
})
export class AuthStateFacade {
  public isAuthorized$ = this.store.select(isUserAuthorized);
  public getToken$ = this.store.select(getToken);
  public getLoginErrorMessage$ = this.store.select(getLoginErrorMessage);
  public getRegisterErrorMessage$ = this.store.select(getRegisterErrorMessage);

  constructor(private store: Store<AuthState>, private sessionStorageService: SessionStorageService, private userStore: Store<UserState>) {}

  login(body: UserModel) {
    this.store.dispatch(requestLogin(body));
  }

  register(body: UserModel) {
    this.store.dispatch(requestRegister(body));
  }

  logout() {
    this.store.dispatch(requestLogout());
    this.userStore.dispatch(requestCurrentUserLogout());
  }

  closeSession() {
    this.store.dispatch(requestLogoutSuccess());
  }

  setAuthorization() {
    this.store.dispatch(requestLoginSuccess({
      token: this.sessionStorageService.getToken()}))
  }
}