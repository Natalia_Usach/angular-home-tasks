import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import { catchError, of, switchMap } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { requestLogin, requestLoginFail, requestLoginSuccess, requestLogout, requestLogoutSuccess, requestRegister, requestRegisterFail, requestRegisterSuccess } from './auth.actions';

@Injectable()
export class LoginEffects {
  login$ = createEffect(() => this.actions$.pipe(
    ofType(requestLogin),
    switchMap((props) =>
      this.authService.login(props)
        .pipe(
          switchMap((data) => {
            this.router.navigate(['courses']);
            const token = data['result'].replace('Bearer ', '');
            return of(requestLoginSuccess({token}));
          }),
          catchError(error => of(requestLoginFail({errorMessage: error})))
        )
    )
  ));

  register$ = createEffect(() => this.actions$.pipe(
    ofType(requestRegister),
    switchMap((props) =>
      this.authService.register(props)
        .pipe(
          switchMap(() => {
            this.router.navigate(['login']);
            return of(requestRegisterSuccess());
          }),
          catchError(error => of(requestRegisterFail()))
        )
    )
  ));

  logout$ = createEffect(() => this.actions$.pipe(
    ofType(requestLogout),
    switchMap(() =>
      this.authService.logout()
        .pipe(
          switchMap(() => {
            this.router.navigate(['login']);
            return of(requestLogoutSuccess());
          }),
          catchError(error => of(error))
        )
    )
  ));

  constructor(private actions$: Actions, private authService: AuthService, private router: Router) {
  }
}
