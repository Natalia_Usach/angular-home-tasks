import { createAction, props } from "@ngrx/store";
import { UserModel } from "src/app/user/user.model";

export const requestLogin = createAction('[AUTH] requestLogin',
  props<UserModel>());
export const requestLoginSuccess = createAction('[AUTH] requestLoginSuccess', props<{token: string}>());
export const requestLoginFail = createAction('[AUTH] requestLoginFail', props<{errorMessage: string}>());
export const requestRegister = createAction('[AUTH] requestRegister',
  props<UserModel>());
export const requestRegisterSuccess = createAction('[AUTH] requestRegisterSuccess');
export const requestRegisterFail = createAction('[AUTH] requestRegisterFail');
export const requestLogout = createAction('[AUTH] requestLogout');
export const requestLogoutSuccess = createAction('[AUTH] requestLogoutSuccess');

