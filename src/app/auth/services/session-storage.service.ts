import { Inject, Injectable } from '@angular/core';
import { WINDOW } from 'window-token';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  constructor(@Inject(WINDOW) private window: Window) {}

  setToken(token: string) {
    this.window.sessionStorage.setItem('token', token);
  }

  getToken(): string {
    return this.window.sessionStorage.getItem('token');
  }

  deleteToken() {
    return this.window.sessionStorage.removeItem('token');
  }
}
