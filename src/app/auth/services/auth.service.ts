import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of, tap } from 'rxjs';
import { UserModel } from 'src/app/user/user.model';
import { SessionStorageService } from './session-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private sessionStorageService: SessionStorageService) { }

  login(user: UserModel) {
    const userData = {
      email: user.email,
      password: user.password
    };
    return this.http.post('http://localhost:3000/login', userData)
      .pipe(
        tap((res) => {
          const token = res['result'].replace('Bearer ', '');
          this.sessionStorageService.setToken(token);
        }),
        catchError(err => of(err))
      );
  }

  logout() {
    return this.http.delete('http://localhost:3000/logout')
      .pipe(
        tap(() => {
          this.sessionStorageService.deleteToken();
        })
      );
  }

  register(user: UserModel) {
    return this.http.post('http://localhost:3000/register', user);
  }
}
