import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthStateFacade } from 'src/app/auth/store/auth.facade';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./../registration/registration.component.scss']
})

export class LoginComponent {

  user = {
    email: "",
    password: ""
  };

  submitted = false;

  isAuthorized$ = this.facade.isAuthorized$;
  loginError$ = this.facade.getLoginErrorMessage$;

  constructor(private facade: AuthStateFacade) {}

  onSubmit(userData: NgForm) {
    this.submitted = true;
    if (userData.form.status === "VALID") {
      this.facade.login(userData.form.value);
    }
  }
}
