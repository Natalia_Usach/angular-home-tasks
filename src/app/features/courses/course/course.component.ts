import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, Observable } from 'rxjs';
import { Course } from 'src/app/models/course.model';
import { CoursesStateFacade } from 'src/app/store/courses/courses.facade';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  id: string;
  courseData: Course;
  authors = [];
  isSingleCourseLoading$: Observable<any>;
  course$: Observable<any>;

  constructor(
    private facade: CoursesStateFacade,
    private router: Router
  ) { }

  ngOnInit() {
    this.isSingleCourseLoading$ = this.facade.isSingleCourseLoading$;
    this.course$ = this.facade.course$.pipe(filter(data => data !== null && data !== undefined));
  }

  getFormattedAuthors(arr?: any) {
    return arr?.map(author => author?.name);
  }

  onGoBackToCoursesClick() {
    this.router.navigate(["courses"]);
  }
}
