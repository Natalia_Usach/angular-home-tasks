import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizedGuard } from 'src/app/auth/guards/authorized.guard';
import { AdminGuard } from 'src/app/user/guards/admin.guard';
import { CourseComponent } from './course/course.component';
import { CourseFormComponent } from './course-form/course-form.component';

import { CoursesComponent } from './courses.component';


const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    canLoad: [AuthorizedGuard],
  },
  {
    path: 'add',
    component: CourseFormComponent,
    canLoad: [AuthorizedGuard],
    canActivate: [AdminGuard],
    pathMatch: 'full'
  },
  {
    path: ':id',
    component: CourseComponent,
    pathMatch: 'full'
  },
  {
    path: 'edit/:id',
    component: CourseFormComponent,
    canLoad: [AuthorizedGuard],
    canActivate: [AdminGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
