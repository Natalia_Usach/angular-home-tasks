import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, filter, Observable, of, take } from 'rxjs';
import { Course } from 'src/app/models/course.model';
import { createNewAuthorValidator } from 'src/app/shared/validators/new-author-validator';
import { AuthorsStateFacade } from 'src/app/store/authors/authors.facade';
import { CoursesStateFacade } from 'src/app/store/courses/courses.facade';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {

  @Input() courseItem: Course;
  @Input() courses: Course[];

  createCourseText = 'Create course';
  createAuthorText = 'Create author';
  deleteAuthorText = 'Delete author';
  editCourseText = 'Edit course';

  form: FormGroup;
  submitted = false;
  authorSubmitted = false;
  courseToEdit: Course;
  id: string;
  shouldBeEditted: boolean;
  allAuthors: any[];
  addedAuthor$: Observable<any>;
  course$: Observable<any>;
  allAuthors$: Observable<any>;
  
  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private coursesFacade: CoursesStateFacade,
    private authorsFacade: AuthorsStateFacade,
    private router: Router
  ) {}

  get title(): AbstractControl {
    return this.form.get("title");
  }

  get description(): AbstractControl {
    return this.form.get("description");
  }

  get authorName(): AbstractControl {
    return this.form.get("authorName");
  }

  get authors() {
    return this.form.controls["authors"] as FormArray;
  }

  get duration(): AbstractControl {
    return this.form.get("duration");
  }

  ngOnInit(): void {
    this.addedAuthor$ = this.authorsFacade.addedAuthor$;
    this.allAuthors$ = this.authorsFacade.authors$;
    this.allAuthors$.subscribe(authors => this.allAuthors = authors);
    this.route.params
      .subscribe(params => {
        this.id = params['id'];
      });

      if (this.route.snapshot.routeConfig.path === 'edit/:id') {
        this.shouldBeEditted = true;
        this.course$ = this.coursesFacade.course$;
        this.course$.pipe(filter(course => !!course)).subscribe(course => {
          this.form = this.fb.group({
            title: [course?.title, Validators.required],
            description: [course?.description, Validators.required],
            authorName: ['', createNewAuthorValidator()],
            duration: [course?.duration, [Validators.required, Validators.min(0)]],
            authors: this.fb.array(course?.authors?.filter(author => !!author)?.map(author => ({value: {name: author?.name, id: author?.id}})))
          });
        });
      } else {
        this.shouldBeEditted = false;
        this.form = this.fb.group({
          title: ['', Validators.required],
          description: ['', Validators.required],
          authorName: ['', createNewAuthorValidator()],
          duration: ['', [Validators.required, Validators.min(0)]],
          authors: this.fb.array([
          ])
        });
      }
  }

  trackById(index: number, author: any): string {
    return author.id;
  }

  onFormSubmit() {
    this.submitted = true;
    const authors = this.form.value.authors.map(item => item.value.id);
    delete this.form.value.authorName;
    const data = {
      title: this.form.value.title,
      description: this.form.value.description,
      duration: this.form.value.duration,
      authors: authors
    };
    if (this.shouldBeEditted) {
      this.coursesFacade.editCourse(data, this.id);
    } else {
      this.coursesFacade.createCourse(data);
    }
  }

  deleteAuthor(id: string, index: number, event: Event) {
    event.preventDefault();
    this.authorsFacade.deleteAuthor(id);
    (<FormArray>this.form.controls['authors']).removeAt(index);
  }

  addAuthor(value: string, event: Event): void {
    this.authorSubmitted = true;
    event.preventDefault();
    if (value && /[a-zA-Z0-9]+/.test(value)) {
      this.authorsFacade.addAuthor(value.trim());
      this.addedAuthor$.pipe(filter(author => author !== null), take(1), catchError(err => of(err))).subscribe((author) => {
        this.authors.push(new FormControl(({value: {name: author.name, id: author.id}})));
        this.authorName.reset();
        this.authorsFacade.resetAddedAuthor();
        this.authorSubmitted = false;
      });
    }
  }

  onCancelClick(event: any) {
    event.preventDefault();
    this.router.navigate(["courses"]);
  }
}
