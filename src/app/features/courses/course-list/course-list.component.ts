import { ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { CoursesStateFacade } from 'src/app/store/courses/courses.facade';
import { Course } from '../../../models/course.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseListComponent {

  @Input() isEditable: boolean;
  @Input() courses: Course[];
  @Input() authors: string[];
  @Output() onDeleteCourse = new EventEmitter();

  faEdit = faEdit;
  faTrash = faTrash;
  showCourseText: string = 'Show course';
  authorNames: string[];

  constructor(private router: Router, private facade: CoursesStateFacade) {
  }

  trackById(index: number, course: Course): string {
    return course.id;
  }

  onShowCourseClick(id: string) {
    this.router.navigate([`courses/${id}`]);
    this.facade.getSingleCourse(id);
  }

  onDeleteCourseClick(id: string) {
    this.onDeleteCourse.emit(id);
  }

  onEditCourseClick(id: string) {
    this.router.navigate([`courses/edit/${id}`]);
    this.facade.getSingleCourse(id);
  }
}
