import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { AuthorsService } from 'src/app/services/authors.service';
import { AuthorsStateFacade } from 'src/app/store/authors/authors.facade';
import { CoursesStateFacade } from 'src/app/store/courses/courses.facade';
import { UserStateFacade } from 'src/app/user/store/user.facade';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  providers: [AuthorsService]
})

export class CoursesComponent implements OnInit, OnDestroy {

  authorsList: any[];
  editable: boolean;
  public loading: boolean;
  shouldFilter: boolean;
  titleText: string = 'Your list is empty';
  infoText: string = `Please use the 'Add new course' button to add your first course`;
  addCourseText: string = 'Add new course';
  private completeSubject$ = new Subject<void>();
  authorNames: any[];
  allCourses$: Observable<any>;
  isAllCoursesLoading$: Observable<any>;
  isCourseListEmpty$: Observable<any>;
  allAuthors$: Observable<any>;
  editable$: Observable<any>;
  filteredCourses$: Observable<any>;

  constructor(
    private router: Router,
    private coursesFacade: CoursesStateFacade,
    private authorsFacade: AuthorsStateFacade,
    private userFacade: UserStateFacade
  ) { }

  ngOnInit(): void {
    this.coursesFacade.getAllCourses();
    this.authorsFacade.getAuthors();
    this.allCourses$ = this.coursesFacade.allCourses$;
    this.editable$ = this.userFacade.isAdmin$;
    this.isAllCoursesLoading$ = this.coursesFacade.isAllCoursesLoading$;
    this.isCourseListEmpty$ = this.coursesFacade.isCourseListEmpty$;
    this.allAuthors$ = this.authorsFacade.authors$;
    this.filteredCourses$ = this.coursesFacade.courses$;
  }

  ngOnDestroy(): void {
    this.completeSubject$.next();
    this.completeSubject$.complete();
  }

  onDeleteCourse(id: string) {
    let answer = window.confirm("Do you really want to delete this course?");
    if (answer) {
      this.coursesFacade.deleteCourse(id);
    }
    return;
  }

  onFindCourse(query: string) {
    this.coursesFacade.getFilteredCourses(query);
    this.shouldFilter = true;
  }

  onAddCourseClick() {
    this.router.navigate(["courses/add"]);
  }

  onClearSearch() {
    this.shouldFilter = false;
  }
}
