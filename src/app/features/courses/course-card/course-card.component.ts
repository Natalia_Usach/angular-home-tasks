import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from '../../../models/course.model';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})

export class CourseCardComponent implements OnInit {
  @Input() courseItem: Course;
  @Input() authors: any[];
  authorNames = [];
  authors$: Observable<any>;

  ngOnInit(): void {
    if(this.courseItem.authors && !this.courseItem.authors.includes(undefined)) {
      this.courseItem.authors.map(obj => this.authorNames.push(obj['name']));
    }
  }
}
