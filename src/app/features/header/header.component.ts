import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserStateFacade } from 'src/app/user/store/user.facade';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userName: string = '';

  constructor(private router: Router, private userFacade: UserStateFacade) { }

  ngOnInit() {
    this.userFacade.getCurrentUser();
  }

  backToHome() {
    this.router.navigate(['courses']);
  }
}
