import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthStateFacade } from 'src/app/auth/store/auth.facade';
import { createEmailValidator } from 'src/app/shared/validators/email-validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {

  form: FormGroup;
  submitted = false;
  registrationError$ = this.facade.getRegisterErrorMessage$;

  constructor(private fb: FormBuilder, private facade: AuthStateFacade) {
    this.form = fb.group({
      name: ["", [
        Validators.required,
        Validators.minLength(6)
      ]],
      email: ["", [Validators.required, createEmailValidator()]],
      password: ["", Validators.required]
    });
  }

  get name(): AbstractControl {
    return this.form.get("name");
  }

  get email(): AbstractControl {
    return this.form.get("email");
  }

  get password(): AbstractControl {
    return this.form.get("password");
  }

  onFormSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      this.facade.register(this.form.value);
    }
  }
}
