import { Action, createReducer, on } from "@ngrx/store";
import { requestCurrentUser, requestCurrentUserFail, requestCurrentUserLogout, requestCurrentUserSuccess } from "./user.actions";

export const userFeatureKey = 'user';

export interface UserState {
  isAdmin: boolean,
  name: string
}

export const initialState: UserState = {
  isAdmin: false,
  name: null
};

export const userRequestReducer = createReducer(
  initialState,
  on(requestCurrentUser, state => ({
    ...state
  })),
  on(requestCurrentUserSuccess, (state, props) => ({
    ...state,
    name: props.name,
    isAdmin: props.isAdmin
  })),
  on(requestCurrentUserFail, (state, props) => ({
    ...state,
    error: props.error
  })),
  on(requestCurrentUserLogout, (state) => ({
    ...state,
    name: null,
    isAdmin: false
  }))
);

export const userReducer = (state: UserState, action: Action): UserState => userRequestReducer(state, action);
  