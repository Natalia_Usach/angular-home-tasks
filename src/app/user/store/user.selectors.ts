import { createFeatureSelector, createSelector } from "@ngrx/store";
import { userFeatureKey, UserState } from "./user.reducer";

export const featureSelector
  = createFeatureSelector<UserState>(userFeatureKey);

export const getName = createSelector(
  featureSelector,
  state => state.name
);

export const isAdmin = createSelector(
  featureSelector,
  state => state.isAdmin
)