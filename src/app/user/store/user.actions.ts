import { createAction, props } from "@ngrx/store";

export const requestCurrentUser = createAction('[USER] requestCurrentUser');
export const requestCurrentUserSuccess = createAction('[USER] requestCurrentUserSuccess', 
props<{ name: string, isAdmin: boolean }>()
);
export const requestCurrentUserFail = createAction('[USER] requestCurrentUserFail', 
props<{ error: string}>());
export const requestCurrentUserLogout = createAction('[USER] requestCurrentUserLogout');