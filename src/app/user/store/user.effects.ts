import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import { catchError, of, switchMap } from 'rxjs';
import { UserService } from '../services/user.service';
import { requestCurrentUser, requestCurrentUserFail, requestCurrentUserSuccess } from './user.actions';

@Injectable()
export class UserEffects {
  getCurrentUser$ = createEffect(() => this.actions$.pipe(
    ofType(requestCurrentUser),
    switchMap(() => 
      this.userService.getUser()
        .pipe(
          switchMap(response => of(requestCurrentUserSuccess({name: response.result.name, isAdmin: response.result.role === "admin" ? true : false}))),
          catchError(error => of(requestCurrentUserFail({error})))
        )
    )
  ));

  constructor(private actions$: Actions, private userService: UserService) {
  }
}
