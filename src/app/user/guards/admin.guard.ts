import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { map, Observable } from 'rxjs';
import { UserStateFacade } from '../store/user.facade';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private userFacade: UserStateFacade) {}

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.userFacade.isAdmin$
      .pipe(
        map((isAdmin: boolean) => {
          if (isAdmin) {
            return true;
          } else {
            this.router.createUrlTree(['/courses']);
            return false;
          }
        })
      );
  }}