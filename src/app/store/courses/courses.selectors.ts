import { createFeatureSelector, createSelector } from "@ngrx/store";
import { coursesFeatureKey, CoursesState } from "./courses.reducer";

export const featureSelector
  = createFeatureSelector<CoursesState>(coursesFeatureKey);

export const isAllCoursesLoadingSelector = createSelector(
  featureSelector,
  state => state.isAllCoursesLoading
);

export const isSearchingStateSelector = createSelector(
  featureSelector,
  state => state.isSearchState
);

export const isSingleCourseLoadingSelector = createSelector(
  featureSelector,
  state => state.isSingleCourseLoading
);

export const getCourses = createSelector(
  featureSelector,
  state => state.courses
);

export const getAllCourses = createSelector(
  featureSelector,
  state => state.allCourses
);

export const getCourse = createSelector(
  featureSelector,
  state => state.course
);

export const getErrorMessage = createSelector(
  featureSelector,
  state => state.errorMessage
);

export const isCourseListEmpty = createSelector(
  featureSelector,
  state => state.isCourseListEmpty
);
