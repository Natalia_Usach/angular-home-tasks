import { Action, createReducer, on } from "@ngrx/store";
import { Course } from "src/app/models/course.model";
import { requestAllCourses, requestAllCoursesFail, requestAllCoursesSuccess, requestBackToCourses, requestCreateCourse, requestCreateCourseFail, requestCreateCourseSuccess, requestDeleteCourse, requestDeleteCourseFail, requestDeleteCourseSuccess, requestEditCourse, requestEditCourseFail, requestEditCourseSuccess, requestFilteredCourses, requestFilteredCoursesSuccess, requestSingleCourse, requestSingleCourseFail, requestSingleCourseSuccess } from "./courses.actions";

export const coursesFeatureKey = 'courses';

export interface CoursesState {
  allCourses: Course[],
  courses: Course[],
  course: Course | null,
  isAllCoursesLoading: boolean,
  isSingleCourseLoading: boolean,
  isSearchState: boolean,
  errorMessage?: string | null,
  isCourseListEmpty: boolean
}

export const initialState: CoursesState = {
  allCourses: [],
  courses: [],
  course: null,
  isAllCoursesLoading: false,
  isSingleCourseLoading: false,
  isSearchState: false,
  errorMessage: null,
  isCourseListEmpty: true
};

export const coursesActionsReducer = createReducer(
  initialState,
  on(requestAllCourses, (state) => {
    return ({
    ...state,
    isAllCoursesLoading: true,
  });}),
  on(requestAllCoursesSuccess, (state, props) => {
    return ({
    ...state,
    allCourses: props.courses,
    isAllCoursesLoading: false,
    isCourseListEmpty: props.courses.length === 0 ? true : false
  });
}),
  on(requestAllCoursesFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage,
    isAllCoursesLoading: false
  })),
  on(requestSingleCourse, state => ({
    ...state,
    isSingleCourseLoading: true
  })),
  on(requestSingleCourseSuccess, (state, props) => ({
    ...state,
    course: props.course,
    isSingleCourseLoading: false
  })),
  on(requestSingleCourseFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage,
    isSingleCourseLoading: false
  })),
  on(requestFilteredCourses, (state) => ({
    ...state,
    isSearchState: true
  })),
  on(requestFilteredCoursesSuccess, (state, props) => ({
    ...state,
    isSearchState: false,
    courses: props.courses
  })),
  on(requestDeleteCourse, (state) => ({
    ...state
  })),
  on(requestDeleteCourseSuccess, (state, props) => {
    return ({
      ...state,
      allCourses: state.allCourses.filter(course => course.id !== props.id),
      isCourseListEmpty: state.allCourses.length === 1 ? true : false
    });
  }),
  on(requestDeleteCourseFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage
  })),
  on(requestEditCourse, (state) => ({
    ...state
  })),
  on(requestEditCourseSuccess, (state) => ({
    ...state
  })),
  on(requestEditCourseFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage
  })),
  on(requestCreateCourse, (state) => ({
    ...state
  })),
  on(requestCreateCourseSuccess, (state) => ({
    ...state
  })),
  on(requestCreateCourseFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage
  })),
  on(requestBackToCourses, (state) => ({
    ...state,
    course: null
  }))
);

export const coursesReducer = (state: CoursesState, action: Action): CoursesState => coursesActionsReducer(state, action);
  