import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Course } from "src/app/models/course.model";
import { requestAllCourses, requestBackToCourses, requestCreateCourse, requestDeleteCourse, requestEditCourse, requestFilteredCourses, requestSingleCourse } from "./courses.actions";
import { CoursesState } from "./courses.reducer";
import { getAllCourses, getCourse, getCourses, getErrorMessage, isAllCoursesLoadingSelector, isCourseListEmpty, isSearchingStateSelector, isSingleCourseLoadingSelector } from "./courses.selectors";


@Injectable({
  providedIn: 'root',
})
export class CoursesStateFacade {
  public isAllCoursesLoading$ = this.store.select(isAllCoursesLoadingSelector);
  public isSingleCourseLoading$ = this.store.select(isSingleCourseLoadingSelector);
  public isSearchingState$ = this.store.select(isSearchingStateSelector);
  public courses$ = this.store.select(getCourses);
  public allCourses$ = this.store.select(getAllCourses);
  public course$ = this.store.select(getCourse);
  public errorMessage$ = this.store.select(getErrorMessage);
  public isCourseListEmpty$ = this.store.select(isCourseListEmpty);

  constructor(private store: Store<CoursesState>) {}

  getAllCourses(): any {
    this.store.dispatch(requestAllCourses());
  }

  getSingleCourse(id: string) {
    this.store.dispatch(requestSingleCourse({id}));
  }

  getFilteredCourses(searchValue: string) {
    this.store.dispatch(requestFilteredCourses({searchValue}));
  }

  editCourse(body: Course, id: string) {
    this.store.dispatch(requestEditCourse({body, id}));
  }

  createCourse(body: Course) {
    this.store.dispatch(requestCreateCourse({body}));
  }

  deleteCourse(id: string) {
    this.store.dispatch(requestDeleteCourse({ id }));
  }

  goBackToCourses() {
    this.store.dispatch(requestBackToCourses());
  }
}