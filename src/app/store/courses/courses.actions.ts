import { createAction, props } from "@ngrx/store";
import { Course } from "src/app/models/course.model";

export const requestAllCourses = createAction('[COURSES] requestAllCourses');
export const requestAllCoursesSuccess = createAction('[COURSES] requestAllCoursesSuccess', props<{courses: Course[]}>());
export const requestAllCoursesFail = createAction('[COURSES] requestAllCoursesFail', props<{errorMessage: string}>());
export const requestSingleCourse = createAction('[COURSES] requestSingleCourse', props<{id: string}>());
export const requestSingleCourseSuccess = createAction('[COURSES] requestSingleCourseSuccess', props<{course: Course}>());
export const requestSingleCourseFail = createAction('[COURSES] requestSingleCourseFail', props<{errorMessage: string}>());
export const requestFilteredCourses = createAction('[COURSES] requestFilteredCourses', props<{searchValue: string}>());
export const requestFilteredCoursesSuccess = createAction('[COURSES] requestFilteredCoursesSuccess', props<{courses: Course[]}>());
export const requestDeleteCourse = createAction('[COURSES] requestDeleteCourse', props<{id: string}>());
export const requestDeleteCourseSuccess = createAction('[COURSES] requestDeleteCourseSuccess', props<{id: string}>());
export const requestDeleteCourseFail = createAction('[COURSES] requestDeleteCourseFail', props<{errorMessage: string}>());
export const requestEditCourse = createAction('[COURSES] requestEditCourse', props<{body: Course, id: string}>());
export const requestEditCourseSuccess = createAction('[COURSES] requestEditCourseSuccess');
export const requestEditCourseFail = createAction('[COURSES] requestEditCourseFail', props<{errorMessage: string}>());
export const requestCreateCourse = createAction('[COURSES] requestCreateCourse', props<{body: Course}>());
export const requestCreateCourseSuccess = createAction('[COURSES] requestCreateCourseSuccess');
export const requestCreateCourseFail = createAction('[COURSES] requestCreateCourseFail', props<{errorMessage: string}>());
export const requestBackToCourses = createAction('[COURSES] requestBackToCourses');

