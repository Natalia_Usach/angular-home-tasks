import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import { catchError, filter, map, of, switchMap, tap } from 'rxjs';
import { CoursesService } from 'src/app/services/courses.service';
import { AuthorsStateFacade } from '../authors/authors.facade';
import { requestAllCourses, requestAllCoursesFail, requestAllCoursesSuccess, requestCreateCourse, requestCreateCourseFail, requestCreateCourseSuccess, requestDeleteCourse, requestDeleteCourseFail, requestDeleteCourseSuccess, requestEditCourse, requestEditCourseFail, requestEditCourseSuccess, requestFilteredCourses, requestFilteredCoursesSuccess, requestSingleCourse, requestSingleCourseFail, requestSingleCourseSuccess } from './courses.actions';
import { CoursesStateFacade } from './courses.facade';

@Injectable()
export class CoursesEffects {
  courses: any;
  getAll$ = createEffect(() => this.actions$.pipe(
    ofType(requestAllCourses),
    switchMap(() => 
      this.coursesService.getAll()
        .pipe(
          switchMap(courses => {
            return this.authorsStateFacade.authors$.pipe(filter(all => all.length !== 0), map((authors: any) => {
              return courses.result.map(course => ({
                title: course.title,
                description: course.description,
                duration: course.duration,
                authors: course.authors
                  .map(id => authors
                    .filter(a => a.id === id)[0]),
                creationDate: course.creationDate,
                id: course.id
              }));
            }));
          }),
          switchMap(data => {
            return of(requestAllCoursesSuccess({courses: data}));
          }),
          catchError(error => of(requestAllCoursesFail({errorMessage: error})))
        )
    )
  ));

  filteredCourses$ = createEffect(() => this.actions$.pipe(
    ofType(requestFilteredCourses),
    switchMap((props) => {
      // let params = new HttpParams();
      // params = params.set('title', props.searchValue).set('description', props.searchValue);
      // return this.coursesService.searchCourse(params)
      // .pipe(
      //   switchMap(courses => {
      //     this.router.navigate(['/courses'], { queryParams: { title: props.searchValue, description: props.searchValue } });
      //     return of(requestFilteredCoursesSuccess({courses: courses['result']}))
      //   }),
      //   catchError(error => of(error))
      // )}
      return this.coursesStateFacade.allCourses$
        .pipe(
          switchMap(courses => {
            const filteredCourses = courses.filter(course => course.title.includes(props.searchValue) || course.description.includes(props.searchValue));
            return of(requestFilteredCoursesSuccess({courses: filteredCourses}));
          }),
          catchError(error => of(error))
        )}
  )));

  getSpecificCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestSingleCourse),
    switchMap((props) => 
      this.coursesService.getCourse(props.id)
        .pipe(
          filter(course => !!course),
          switchMap(course => {
            return this.authorsStateFacade.authors$.pipe(filter(all => !!all), map(authors => {
              const courseItem = course.result;
              return {
                title: courseItem.title,
                description: courseItem.description,
                duration: courseItem.duration,
                authors: courseItem.authors.map(authorId => authors.filter(author => author.id === authorId)[0]),
                creationDate: courseItem.creationDate,
                id: courseItem.id
              };
            }));
          }),
          switchMap((result) => of(requestSingleCourseSuccess({ course: result }))),
          catchError(error => of(requestSingleCourseFail({errorMessage: error})))
        ))
  ));

  deleteCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestDeleteCourse),
    switchMap((props) => 
      this.coursesService.deleteCourse(props.id)
        .pipe(
          switchMap(() => of(requestDeleteCourseSuccess({id: props.id}))),
          catchError(error => of(requestDeleteCourseFail({errorMessage: error})))
        ))
  ));

  editCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestEditCourse),
    tap((props) => {
      this.router.navigate([`courses/edit/${props.id}`]);
    }),
    switchMap((props) => 
      this.coursesService.editCourse(props.body, props.id)
        .pipe(
          switchMap(() => {
            this.router.navigate(['courses']);
            return of(requestEditCourseSuccess());
          }),
          catchError(error => of(requestEditCourseFail({errorMessage: error})))
        ))
  ));

  createCourse$ = createEffect(() => this.actions$.pipe(
    ofType(requestCreateCourse),
    switchMap((props) => 
      this.coursesService.addCourse(props.body)
        .pipe(
          switchMap(() => {
            this.router.navigate(['courses']);
            return of(requestCreateCourseSuccess());
          }),
          catchError(error => of(requestCreateCourseFail({errorMessage: error})))
        ))
  ));

  redirectToTheCoursesPage$ = createEffect(() => this.actions$.pipe(
    ofType(requestCreateCourseSuccess, requestEditCourseSuccess, requestSingleCourseFail),
    map(() => {
      this.router.navigate(['courses']);
    })
  ), {dispatch: false});

  constructor(private actions$: Actions, private coursesService: CoursesService, private authorsStateFacade: AuthorsStateFacade, private coursesStateFacade: CoursesStateFacade, private router: Router) {
  }
}
