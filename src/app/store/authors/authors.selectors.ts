import { createFeatureSelector, createSelector } from "@ngrx/store";
import { authorsFeatureKey, AuthorsState } from "./authors.reducer";

export const featureSelector
  = createFeatureSelector<AuthorsState>(authorsFeatureKey);

export const getAddedAuthors = createSelector(
  featureSelector,
  state => state.addedAuthor
);

export const getAuthors = createSelector(
  featureSelector,
  state => state.authors
);