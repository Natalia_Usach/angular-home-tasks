import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { requestAddAuthor, requestAuthors, requestDeleteAuthor, resetAddedAuthor } from "./authors.actions";
import { AuthorsState } from "./authors.reducer";
import { getAddedAuthors, getAuthors } from "./authors.selectors";

@Injectable({
  providedIn: 'root',
})
export class AuthorsStateFacade {
  public addedAuthor$ = this.store.select(getAddedAuthors);
  public authors$ = this.store.select(getAuthors);

  constructor(private store: Store<AuthorsState>) {}

  getAuthors() {
    this.store.dispatch(requestAuthors());
  }

  addAuthor(author: string) {
    this.store.dispatch(requestAddAuthor({name: author}));
  }

  resetAddedAuthor() {
    this.store.dispatch(resetAddedAuthor());
  }

  deleteAuthor(id: string) {
    this.store.dispatch(requestDeleteAuthor({id}));
  }
}