import { Action, createReducer, on } from "@ngrx/store";
import { Author } from "src/app/models/author.model";
import { requestAddAuthor, requestAddAuthorFail, requestAddAuthorSuccess, requestAuthors, requestAuthorsFail, requestAuthorsSuccess, requestDeleteAuthor, requestDeleteAuthorFail, requestDeleteAuthorSuccess, resetAddedAuthor } from "./authors.actions";

export const authorsFeatureKey = 'authors';

export interface AuthorsState {
  authors: Author[];
  addedAuthor: Author | null,
  errorMessage: string
}

export const initialState: AuthorsState = {
  authors: [],
  addedAuthor: null,
  errorMessage: null
};

export const authorsActionsReducer = createReducer(
  initialState,
  on(requestAuthors, state => ({
    ...state
  })),
  on(requestAuthorsSuccess, (state, props) => ({
    ...state,
    authors: props.authors['result']
  })),
  on(requestAuthorsFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage
  })),
  on(requestAddAuthor, (state, props) => ({
    ...state,
    name: props.name
  })),
  on(requestAddAuthorSuccess, (state, props) => ({
    ...state,
    addedAuthor: props,
    authors: [...state.authors, { name: props.name, id: props.id}]
  })),
  on(requestAddAuthorFail, (state, props) => ({
    ...state,
    errorMessage: props.errorMessage
  })),
  on(resetAddedAuthor, (state) => ({
    ...state,
    addedAuthor: null
  })),
  on(requestDeleteAuthor, (state, props) => ({
    ...state,
    id: props.id
  })),
  on(requestDeleteAuthorSuccess, (state, props) => ({
    ...state,
    authors: state.authors.filter(a => a.id !== props.id)
  })),
  on(requestDeleteAuthorFail, (state) => ({
    ...state,
    errorMessage: 'Smth went wrong with deleting an author'
  }))
);

export const authorsReducer = (state: AuthorsState, action: Action): AuthorsState => authorsActionsReducer(state, action);
  