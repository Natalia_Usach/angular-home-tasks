import { createAction, props } from "@ngrx/store";
import { Author } from "src/app/models/author.model";

export const requestAuthors = createAction('[AUTHORS] requestAuthors');
export const requestAuthorsSuccess = createAction('[AUTHORS] requestAuthorsSuccess', props<{authors: Author[]}>());
export const requestAuthorsFail = createAction('[AUTHORS] requestAuthorsFail', props<{errorMessage: string}>());
export const requestAddAuthor = createAction('[AUTHORS] requestAddAuthor', props<{name: string}>());
export const requestAddAuthorSuccess = createAction('[AUTHORS] requestAddAuthorSuccess', props<Author>());
export const requestAddAuthorFail = createAction('[AUTHORS] requestAddAuthorFail', props<{errorMessage: string}>());
export const resetAddedAuthor = createAction('[AUTHORS] resetAddedAuthor');
export const requestDeleteAuthor = createAction('[AUTHORS] requestDeleteAuthor', props<{id: string}>());
export const requestDeleteAuthorSuccess = createAction('[AUTHORS] requestDeleteAuthorSuccess', props<{id: string}>());
export const requestDeleteAuthorFail = createAction('[AUTHORS] requestDeleteAuthorFail');
