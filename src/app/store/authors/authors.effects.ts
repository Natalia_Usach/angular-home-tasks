import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import { catchError, of, switchMap } from 'rxjs';
import { AuthorsService } from 'src/app/services/authors.service';
import { requestAddAuthor, requestAddAuthorFail, requestAddAuthorSuccess, requestAuthors, requestAuthorsFail, requestAuthorsSuccess, requestDeleteAuthor, requestDeleteAuthorFail, requestDeleteAuthorSuccess } from './authors.actions';

@Injectable()
export class AuthorsEffects {
  getAuthors$ = createEffect(() => {
    return this.actions$.pipe(
    ofType(requestAuthors),
    switchMap(() => 
      this.authorsService.getAll()
        .pipe(
          switchMap((authors: any) => {
            return of(requestAuthorsSuccess({authors}));
          }),
          catchError(error => of(requestAuthorsFail({errorMessage: error})))
        )
    )
  )});

  addAuthor$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(requestAddAuthor),
      switchMap((props) => {
        return this.authorsService.addAuthor(props.name)
          .pipe(
            switchMap((author: any) => {
              return of(requestAddAuthorSuccess(author['result']));
            }),
            catchError(error => of(requestAddAuthorFail({errorMessage: error})))
          )
      })
    );
  });

  deleteAuthor$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(requestDeleteAuthor),
      switchMap((props) => {
        return this.authorsService.deleteAuthor(props.id)
          .pipe(
            switchMap(() => of(requestDeleteAuthorSuccess({id: props.id}))),
            catchError(error => of(requestDeleteAuthorFail()))
          )
      })
    );
  });

  constructor(private actions$: Actions, private authorsService: AuthorsService) {
  }
}
