import { ActionReducerMap } from "@ngrx/store";
import { LoginEffects } from "../auth/store/auth.effects";
import { authFeatureKey, authReducer, AuthState } from "../auth/store/auth.reducer";
import { UserEffects } from "../user/store/user.effects";
import { userFeatureKey, userReducer, UserState } from "../user/store/user.reducer";
import { AuthorsEffects } from "./authors/authors.effects";
import { authorsFeatureKey, authorsReducer, AuthorsState } from "./authors/authors.reducer";
import { CoursesEffects } from "./courses/courses.effects";
import { coursesFeatureKey, coursesReducer, CoursesState } from "./courses/courses.reducer";

interface State {
  [authFeatureKey]: AuthState;
  [coursesFeatureKey]: CoursesState;
  [authorsFeatureKey]: AuthorsState;
  [userFeatureKey]: UserState
}

export const reducers: ActionReducerMap<State> = {
  [authFeatureKey]: authReducer,
  [coursesFeatureKey]: coursesReducer,
  [authorsFeatureKey]: authorsReducer,
  [userFeatureKey]: userReducer
};

export const effects = [LoginEffects, CoursesEffects, AuthorsEffects, UserEffects];
